# project quick-test-task

* developer: [Svetlana Kostina].

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).
---
## Инструкция по запуску проекта:

Клонируйте проект на свой компьютер:

```sh
git clone git@src.sibext.com:test-frontend-5/quick-test-task.git
```
 
Переключитесь в ветку kostina-svetlana-20180823:

```sh  
git checkout kostina-svetlana-20180823
```

Установите зависимости: 

## `npm install`



Из директории проекта можно запустить следующие команды:

## `npm start`
  
Запускает приложение в режиме разработки.<br>
Откройте [http://localhost:3000](http://localhost:3000) в браузере, чтобы увидеть результат запуска приложения.

В случае внесения правок, страница автоматически перезагружается.<br>
В консоль выводятся ошибки из линтера.


## `npm run build`

Собирает приложение для продакшена в папку `build`.<br>


